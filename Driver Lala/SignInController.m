//
//  SignInController.m
//  Driver Lala
//
//  Created by Apple on 01/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//


#import "SignInController.h"

@interface SignInController ()

@end

@implementation SignInController

@synthesize tfPhoneNumber, tfPassword, objCustomProtocol;
@synthesize dictionaryObject;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"hello i am view did load");
    
    self.objCustomProtocol = [[CustomProtocol alloc] init];
    self.objCustomProtocol.delegate = self;
    
    [self.objCustomProtocol activityIndicator:self.view :true];
    
    self.tfPhoneNumber.delegate = self.tfPassword.delegate = self;
    
}// end view did load

//pgrm mark ---- -----

// declare delegate for text field
// Remove the keyboard when click to retun button

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"Method : textFieldShouldReturn Resign first responder");
    [textField resignFirstResponder];
    return NO;
}

// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    NSLog(@"hello i am viewWillAppear");

    //[UIApplication sharedApplication].networkActivityIndicatorVisible = true;

    

}// end viewWillAppear

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"viewDidAppear");

}

// pgrm mark ----- -------

- (IBAction)btnLoginAction:(UIButton *)sender {
    NSLog(@"Login button pressed");
    
    [self.objCustomProtocol activityIndicator:self.view :false];
    
    NSString *phoneNumber = self.tfPhoneNumber.text;
    NSString *password = self.tfPassword.text;
    
    NSLog(@"Phone nubmer %@ and password is %@",phoneNumber,password);
    
    if(self.tfPhoneNumber == nil || [self.tfPhoneNumber.text isEqualToString:@""]
       || self.tfPassword == nil || [self.tfPassword.text isEqualToString:@""]) {
        
        [self.objCustomProtocol  showAlertMessage:@"Please fill Username/Password"];
        
    }// end if
    else {
        
        [self callToDelegateForAuthentication];
        
    }// end else
    
}// end btn login action

// pgrm mark ---- -----
// contact to server and get response data

- (void) callToDelegateForAuthentication {
    
    // Check authentication for user
    [objCustomProtocol checkAuthentication:self.tfPhoneNumber.text :self.tfPassword.text];
    
}// end of method callToDelegateForAuthentication

// Analysis of authentication results get from protocol delegate

- (void) authenticationResults:(NSData *) authResultData {
    
    @try {
        
        NSError *error=nil;
        
        // Convert JSON Object into Dictionary
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:authResultData
                                                             options:NSJSONReadingMutableContainers
                                                             error:&error];
        
        // Get your object "Docs" from the root object
        dictionaryObject = (NSDictionary *)[JSON objectForKey:@"responseData"];
        
        // Get Parameters from Dictionary object
        NSString *statusCode = [JSON objectForKey:@"statusCode"];
        
        if([statusCode isEqualToString:@"500"]) {
            
            // Show alert pop for incorrect access
            [self.objCustomProtocol showAlertMessage:@"Incorrect username or passowrd"];
            
        }else {
            
            // Show alert pop for incorrect access
            //[self.objCustomProtocol showAlertMessage:@"Logged in successfully!!!"];
            [self performSegueWithIdentifier:@"dashboard_tab_segue" sender:self];
            
            
        }
        
    } @catch(NSException *theException) {
        
        NSLog(@"An exception occurred: %@", theException.name);
        NSLog(@"Here are some details: %@", theException.reason);
        
    } @finally {
        NSLog(@"Method: authenticationResults Executing finally block");
    }
    
}// End of Method authenticationResults

// pgrm mark ----- -------

- (IBAction)btnRegisterAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"signup_segue" sender:self];
    
}// end btn register action

// pgrm mark ----- -------
// select the city location

- (IBAction)btnSelectLocationAction:(UIButton *)sender {
    
    NSString *actionSheetTitle = @"Select Location"; //Action Sheet Title
    //NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"United states of America";
    NSString *other2 = @"India";
    NSString *other3 = @"United kingdom";
    NSString *other4 = @"Australia";
    NSString *cancelTitle = @"Cancel";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, other3, other4, nil];
    
    [actionSheet showInView:self.view];
    
}// end btn seleect location

// pgrm mark ----- -----

// delegate for action sheet

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSLog(@"button index %ld",(long)buttonIndex);
    
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if  (buttonIndex == 0) {
        NSLog(@"Destructive pressed --> Delete Something");
    }
    if ([buttonTitle isEqualToString:@"United states of America"]) {
        NSLog(@"Other 1 pressed");
    }
    if ([buttonTitle isEqualToString:@"India"]) {
        NSLog(@"Other 2 pressed");
    }
    if ([buttonTitle isEqualToString:@"United kingdom"]) {
        NSLog(@"Other 3 pressed");
    }
    
    if ([buttonTitle isEqualToString:@"Australia"]) {
        NSLog(@"Other 4 pressed");
    }

    if ([buttonTitle isEqualToString:@"Cancel Button"]) {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
    
}// end action sheet

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
