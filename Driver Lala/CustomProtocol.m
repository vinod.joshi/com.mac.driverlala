//
//  SpineCustomProtocol.m
//  Spine MD
//
//  Created by arroWebs Inc on 17/10/13.
//  Copyright (c) 2013 arroWebs Inc. All rights reserved.
//

#import "CustomProtocol.h"

@implementation CustomProtocol

// Set Global values for the Application

- (void) setUserDefaults:(NSString *) setObject : (NSString *) forKey{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:setObject forKey:forKey];
    [defaults synchronize];
    
}// end of Method setUserDefaults

// Get Global Values from Application

- (NSString *) getUserDefaults:(NSString *) objectForKey{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *resultObject = [defaults objectForKey:objectForKey];
    
    return resultObject;
}// End of Method getUserDefaults

// This method will check authentication of user/patients

- (NSString *) checkAuthentication:(NSString * ) phoneNumber : (NSString *) password {
    
    NSLog(@"Class:CustomProtocal Method:checkAuthentication");
    
    NSString *siteURLstr = [NSString stringWithFormat:@"http://192.168.1.14/webclient/static_api.php?action=login"];
   
    // Get result into parameters from dispatch_async
    NSData *jsonResult;
    
    // Get data from another thread
    jsonResult = [self JSONHandler:siteURLstr];
    
    // Send jsonResult to another class
    [self.delegate authenticationResults:jsonResult];
    
    return nil;
}// end of method checkAuthentication

// This method will handle all type of JSON Parameters will return NSData value

- (NSData * ) JSONHandler:(NSString *)siteURL {
    
    @try {
        
        NSURL *url=[NSURL URLWithString:siteURL];
        
        NSData *data=[NSData dataWithContentsOfURL:url];
        
        NSLog(@"JSON Data has been executued successfully");
        
        return data;
        
    } @catch(NSException *theException) {
        
        NSLog(@"An exception occurred: %@", theException.name);
        NSLog(@"Here are some details: %@", theException.reason);
        
    } @finally {
        NSLog(@"Class:SpineCustomProtocal Method:JSONHandler Executing finally block");
    }
    
    
}// end of method JSONHandler

// This method will give parameter value for plist

- (NSString *) getPlistVars:(NSString *) keyName{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"careCustom" ofType:@"plist"];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    NSString * spine_var = [dict objectForKey:keyName] ;
    
    NSLog(@"Method: getPlistVars %@",spine_var);
    
    return spine_var;
}// end of method getPlistVars

// Check is network available in iPhone/iPad

- (NetworkStatus) checkNetworkAvailability {
    
    Reachability* wifiReach = [Reachability reachabilityWithHostName: @"www.apple.com"];
    NetworkStatus remoteHostStatus = [wifiReach currentReachabilityStatus];
    
    switch (remoteHostStatus) {
        case NotReachable: {
            NSLog(@"Access Not Available");
            break;
        }
        case ReachableViaWWAN: {
            NSLog(@"Reachable WWAN");
            break;
        }
        case ReachableViaWiFi: {
            NSLog(@"Reachable WiFi");
            break;
        }
    }
    return remoteHostStatus;
    
}// end checkNetworkAvailability

// Show error alert as
- (void) showAlertMessage:(NSString *) message {
    
    UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
    [ErrorAlert show];
    
}// end of method showErrorAlert

- (void) activityIndicator:(UIView *)view :(BOOL)status {
    
    // this will be shown in status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = status;
    
    // this indicator will be show in self.view view controller
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.center = CGPointMake(view.frame.size.width / 2.0
                                           , view.frame.size.height / 2.0);
    
    [view addSubview: activityIndicator];
    
    if(status) {
        [activityIndicator startAnimating];
    } else {
        NSLog(@"stop animationg");
        [activityIndicator stopAnimating];
    }
    
    
}// end showActivityIndicator


@end
