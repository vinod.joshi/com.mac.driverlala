//
//  SignInController.h
//  Driver Lala
//
//  Created by Apple on 01/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomProtocol.h"

@interface SignInController : UIViewController < UINavigationControllerDelegate
, UIActionSheetDelegate, CustomDelegate , UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@property (nonatomic,retain) CustomProtocol *objCustomProtocol;
@property (nonatomic,retain) NSDictionary *dictionaryObject;

@end
